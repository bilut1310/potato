import argparse
import binascii
import getpass
import hashlib
import os
from typing import BinaryIO, Iterator, Tuple
from itertools import cycle

BASE64_TABLE = b'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/'
I2B = dict(enumerate(BASE64_TABLE))
B2I = dict((b, i) for i, b in enumerate(BASE64_TABLE))


class Potato(object):
    def __init__(self, data_dir: str, text_dir: str, batch_size=0):
        self.data_dir = data_dir
        self.text_dir = text_dir
        self.batch_size = batch_size * 1024

        key = getpass.getpass('Key: ')
        self.key = cycle(self.base64_encode(key.encode()))

        self._n = 0
        self._text_buf: BinaryIO
        if self.batch_size:
            self.write_text = self.write_text_batch
        else:
            self.write_text = self.write_text_all

    @staticmethod
    def base64_encode(d: bytes) -> bytes:
        padding = -len(d) % 3
        r = binascii.b2a_base64(d, newline=False)
        if padding:
            r = r[:-padding]
        return r

    @staticmethod
    def base64_decode(d: bytes) -> bytes:
        padding = -len(d) % 4
        d += b'=' * padding
        return binascii.a2b_base64(d.decode())

    def read_data(self) -> Iterator[Tuple[str, bytes]]:
        for fn in os.listdir(self.data_dir):
            fp = os.path.join(self.data_dir, fn)
            if not os.path.isfile(fp):
                continue
            with open(fp, 'rb') as buf:
                yield fn, buf.read()

    def write_data(self, fname: str, data: bytes):
        fp = os.path.join(self.data_dir, fname)
        with open(fp, 'wb') as buf:
            buf.write(data)

    def next_text_fp(self) -> str:
        self._n += 1
        fname = f'potato_{self._n:03d}.txt'
        return os.path.join(self.text_dir, fname)

    def read_text(self, size: int) -> bytes:
        if not hasattr(self, '_text_buf'):
            fp = self.next_text_fp()
            if not os.path.isfile(fp):
                return b''
            self._text_buf = open(fp, 'rb')

        text = self._text_buf.read(size)
        if len(text) < size:
            self._text_buf.close()
            del self._text_buf
            return text + self.read_text(size - len(text))
        else:
            return text

    def write_text_all(self, text: bytes):
        fp = self.next_text_fp()
        with open(fp, 'wb') as buf:
            buf.write(text)

    def write_text_batch(self, text: bytes):
        if not hasattr(self, '_text_buf'):
            fp = self.next_text_fp()
            self._text_buf = open(fp, 'wb')

        cur_pos = self._text_buf.tell()
        size = self.batch_size - cur_pos
        text, pending_text = text[:size], text[size:]
        self._text_buf.write(text)

        if self._text_buf.tell() == self.batch_size:
            self._text_buf.close()
            del self._text_buf

        if pending_text:
            self.write_text_batch(pending_text)

    def encrypt(self, data: bytes) -> bytes:
        return bytes(I2B[(B2I[d] + B2I[k]) % 64] for k, d in zip(self.key, data))

    def decrypt(self, data: bytes) -> bytes:
        return bytes(I2B[(64 + B2I[d] - B2I[k]) % 64] for k, d in zip(self.key, data))

    def encode(self):
        if not os.path.isdir(self.text_dir):
            os.makedirs(self.text_dir)

        for fn, data in self.read_data():
            fn = self.base64_encode(fn.encode())
            fn = self.encrypt(fn)
            fn_len = f'{len(fn):04d}'.encode()
            data = self.base64_encode(data)
            data = self.encrypt(data)
            data_len = f'{len(data):016d}'.encode()

            self.write_text(fn_len + fn + data_len + data)

    def decode(self):
        if not os.path.isdir(self.data_dir):
            os.makedirs(self.data_dir)

        while True:
            fn_len = self.read_text(4)
            if not fn_len:
                break
            fn_len = int(fn_len)
            fn = self.read_text(fn_len)
            fn = self.decrypt(fn)
            fn = self.base64_decode(fn).decode()
            data_len = int(self.read_text(16))
            data = self.read_text(data_len)
            data = self.decrypt(data)
            data = self.base64_decode(data)
            print(fn, len(data))
            self.write_data(fn, data)


class HelpFormatter(argparse.ArgumentDefaultsHelpFormatter):
    def __init__(self, prog):
        super().__init__(prog, max_help_position=80)

    def _format_action_invocation(self, action):
        if not action.option_strings or action.nargs == 0:
            return super()._format_action_invocation(action)
        default = self._get_default_metavar_for_optional(action)
        args_string = self._format_args(action, default)
        return ', '.join(action.option_strings) + ' ' + args_string

    def _format_actions_usage(self, actions, groups):
        # split optionals from positionals
        optionals = []
        positionals = []
        for action in actions:
            if action.option_strings:
                optionals.append(action)
            else:
                positionals.append(action)
        return super()._format_actions_usage(positionals + optionals, groups)


def main():
    parser = argparse.ArgumentParser(
        description='This is just a potato\U0001F954.', formatter_class=HelpFormatter
    )
    parser.add_argument(
        'command', choices=['encode', 'decode'], help='encode to texts or decode from texts.'
    )
    parser.add_argument('input', metavar='INPUT', help='directory of the input files.')
    parser.add_argument('-o', '--output', default='output', help='directory of the output files.')
    parser.add_argument(
        '-b',
        '--batch-size',
        metavar='SIZE',
        type=int,
        default=0,
        help='specify the batch size of output texts in KB.',
    )
    args = parser.parse_args()

    if args.command == 'encode':
        potato = Potato(args.input, args.output, batch_size=args.batch_size)
        potato.encode()
    elif args.command == 'decode':
        potato = Potato(args.output, args.input)
        potato.decode()


if __name__ == '__main__':
    main()
